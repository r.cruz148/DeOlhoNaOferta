# coding=UTF8
__author__ = 'Ronan Cruz'

import bottle
import pymongo

from model.OfertaService import OfertaService
from view.View import View


class OfertaController(object):

    def __init__(self, db, view):
        super(OfertaController, self).__init__()
        self.__service = OfertaService(db)
        self.__view = view

    def rotear(self):

        @bottle.get('/oferta/<id>')
        def getOferta(id):
            result = self.__service.buscarPorId(id)
            self.__view.atualizar(
                self.__service.getStatus(),
                self.__service.getMensagem(),
                result
            )
            return self.__view.gerarJSONCompleto()

        @bottle.post('/oferta')
        def inserirOferta():
            self.__service.inserir(
                bottle.request.forms.get('titulo'),
                bottle.request.forms.get('descricao'),
                bottle.request.forms.get('preco'),
                bottle.request.forms.get('imagem'),
                bottle.request.forms.get('mercado'),
                bottle.request.forms.get('latitude'),
                bottle.request.forms.get('longitude'),
                bottle.request.forms.get('nome'),
                bottle.request.forms.get('email')
            )
            self.__view.atualizar(
                self.__service.getStatus(),
                self.__service.getMensagem()
            )
            return self.__view.gerarJSONSimples()

        @bottle.get('/ofertas')
        def listarOfertas():
            result = self.__service.buscar()
            self.__view.atualizar(
                self.__service.getStatus(),
                self.__service.getMensagem(),
                result
            )
            return self.__view.gerarJSONCompleto()

