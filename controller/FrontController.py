# coding=UTF8
__author__ = 'Ronan Ribeiro'

import pymongo
import bottle

from UsuarioController import UsuarioController
from OfertaController import OfertaController
from view.View import View

class FrontController(object):

    def __init__(self):
        super(FrontController, self).__init__()

        view = View()
        con_string = 'mongodb://localhost'
        con = pymongo.MongoClient(con_string)
        db = con.banco_ofertas

        ofertaCtrl = OfertaController(db, view)
        usuarioCtrl = UsuarioController(db, view)

        ofertaCtrl.rotear()
        usuarioCtrl.rotear()

        bottle.response.content_type = 'application/json; charset=UTF8'
        bottle.run(host='localhost', port=8082, debug=True)

if __name__ == '__main__':
    frontCtrl = FrontController()