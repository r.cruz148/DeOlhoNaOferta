# coding=UTF8
__author__ = 'Ronan Ribeiro'

from model.UsuarioService import UsuarioService
import bottle

class UsuarioController(object):

    def __init__(self, db, view):
        super(UsuarioController, self).__init__()
        self.__service = UsuarioService(db)
        self.__view = view

    def rotear(self):
        @bottle.post('/logar')
        def logar():
            result = self.__service.logar(
                bottle.request.forms.get('email'),
                bottle.request.forms.get('senha'),
            )
            self.__view.atualizar(
                self.__service.getStatus(),
                self.__service.getMensagem(),
                result
            )
            return self.__view.gerarJSONCompleto()

        @bottle.post('/usuario')
        def inserirUsuario():
            self.__service.inserir(
                bottle.request.forms.get('nome'),
                bottle.request.forms.get('sobrenome'),
                bottle.request.forms.get('email'),
                bottle.request.forms.get('senha'),
            )
            self.__view.atualizar(
                self.__service.getStatus(),
                self.__service.getMensagem()
            )
            return self.__view.gerarJSONSimples()