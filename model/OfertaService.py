# coding=UTF8
__author__ = 'Ronan Cruz'

from OfertaDAO import OfertaDAO
from Oferta import Oferta
from Localizacao import Localizacao
from Usuario import Usuario
from bson.objectid import ObjectId

class OfertaService(object):

    def __init__(self, db):
        super(OfertaService, self).__init__()
        self.__dao = OfertaDAO(db)
        self.__mensagem = ''
        self.__status = False

    def getMensagem(self):
        return self.__mensagem

    def getStatus(self):
        return self.__status

    def inserir(self, titulo, descricao, preco, imagem, mercado, latitude, longitude, nome, email):

        oferta = Oferta(titulo, descricao, preco, imagem, mercado, Localizacao(latitude, longitude), Usuario(nome, email))

        try:
            self.__dao.inserir(oferta)
            self.__mensagem = 'Oferta publicada com sucesso!'
        except:
            self.__mensagem = 'Não foi possível publicar a oferta.'
            self.__status = False

        self.__status = True

    def buscar(self):

        result = None

        try:
            result = self.__dao.buscar()
            self.__mensagem = 'Ofertas encontradas.'
        except:
            self.__mensagem = 'Erro ao localizar as ofertas.'
            self.__status = False

        self.__status = True

        return result

    def buscarPorId(self, id):

        result = None
        query = {'_id': ObjectId(id)}
        print query
        try:
            result = self.__dao.buscarPorId(query)
            self.__mensagem = 'Oferta encontrada.'
        except:
            self.__mensagem = 'Erro ao localizar as ofertas.'
            self.__status = False

        self.__status = True

        return result
