# coding=UTF8
__author__ = 'Ronan Ribeiro'

class UsuarioDAO(object):

    def __init__(self, db):
        super(UsuarioDAO, self).__init__()
        self.__db = db
        self.__usuarios = db.usuarios

    def inserir(self, usuario):

        doc = {
            '_id' : usuario.getEmail(),
            'nome' : usuario.getNome(),
            'sobrenome' : usuario.getSobrenome(),
            'senha' : usuario.getSenha()
        }

        self.__usuarios.insert_one(doc)

    def buscarPorEmail(self, email):
        query = {
            '_id' : email
        }
        result = self.__usuarios.find_one(query)
        return result