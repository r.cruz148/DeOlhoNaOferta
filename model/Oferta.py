# coding=UTF8
__author__ = 'Ronan Cruz'

import datetime

class Oferta(object):

    def __init__(self, titulo, descricao, preco, imagem, mercado, localizacao, usuario, id=''):
        super(Oferta, self).__init__()
        self.__id = id
        self.__titulo = titulo
        self.__descricao = descricao
        self.__preco = preco
        self.__imagem = imagem
        self.__mercado = mercado
        self.__localizacao = localizacao
        self.__usuario = usuario
        self.__dataHora = datetime.datetime.now()

    def getId(self):
        return self.__id

    def getTitulo(self):
        return self.__titulo

    def getDescricao(self):
        return self.__descricao

    def getPreco(self):
        return self.__preco

    def getImagem(self):
        return self.__imagem

    def getMercado(self):
        return self.__mercado

    def getLocalizacao(self):
        return self.__localizacao

    def getUsuario(self):
        return self.__usuario

    def getDataHora(self):
        return self.__dataHora