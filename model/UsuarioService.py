# coding=UTF8
__author__ = 'Ronan Ribeiro'

from UsuarioDAO import UsuarioDAO
from Usuario import Usuario

class UsuarioService(object):

    def __init__(self, db):
        super(UsuarioService, self).__init__()
        self.__dao = UsuarioDAO(db)
        self.__mensagem = ''
        self.__status = False

    def getMensagem(self):
        return self.__mensagem

    def getStatus(self):
        return self.__status

    def inserir(self, nome, sobrenome, email, senha):

        usuario = Usuario(nome, email, senha, sobrenome)

        try:
            self.__dao.inserir(usuario)
            self.__mensagem = 'Usuário cadastrado com sucesso!'
        except:
            self.__mensagem = 'Não foi possível efetuar o cadastro.'
            self.__status = False

        self.__status = True

    def buscarPorEmail(self, email):

        result = None
        try:
            result = self.__dao.buscarPorEmail(email)
            if result == None:
                self.__mensagem = 'Email não cadastrado.'
                self.__status = False
            else:
                self.__mensagem = 'Usuario cadastrado.'
                self.__status = True
        except:
            self.__mensagem = 'Não foi possível localizar usuário.'
            self.__status = False

        return result

    def logar(self, email, senha):
        result = self.buscarPorEmail(email)
        if (result != None):
            if self.__status == True:
                usuario = Usuario(
                    result['nome'],
                    result['_id'],
                    result['senha'],
                    result['sobrenome']
                )
                self.__status = usuario.validarSenha(senha)
                if self.__status == True:
                    self.__mensagem = 'Login efetuado com sucesso!'
                else:
                    self.__mensagem = 'Senha incorreta!'
                    return None

            result.pop('senha')
            return result