# coding=UTF8
__author__ = 'Ronan Cruz'

class Usuario(object):

    def __init__(self, nome, email, senha='', sobrenome=''):
        super(Usuario, self).__init__()
        self.__email = email
        self.__nome = nome
        self.__sobrenome = sobrenome
        self.__senha = senha

    def getEmail(self):
        return self.__email

    def getNome(self):
        return self.__nome

    def getSobrenome(self):
        return self.__sobrenome

    def getSenha(self):
        return self.__senha

    def validarSenha(self, senha):
        if self.__senha == senha:
            return True
        return False