# coding=UTF8
__author__ = 'Ronan Cruz'

import pymongo

class OfertaDAO(object):

    def __init__(self, db):
        super(OfertaDAO, self).__init__()
        self.__db = db
        self.__ofertas = db.ofertas

    def inserir(self, oferta):

        doc  = {
            'titulo' : oferta.getTitulo(),
            'descricao' : oferta.getDescricao(),
            'preco' : oferta.getPreco(),
            'imagem' : oferta.getImagem(),
            'mercado' : oferta.getMercado(),
            'localizacao' : {
                'latitude' : oferta.getLocalizacao().getLatitude(),
                'longitude' : oferta.getLocalizacao().getLongitude()
            },
            'usuario' : {
                'nome' : oferta.getUsuario().getNome(),
                'email' : oferta.getUsuario().getEmail()
            },
            'dataHora' : oferta.getDataHora()
        }

        self.__ofertas.insert_one(doc)

    def buscar(self, query={}, proj={}):
        result = None
        if proj == {}:
            result = self.__ofertas.find(query)
        else:
            result = self.__ofertas.find(query, proj)
        return result

    def buscarPorId(self, query={}):
        result = self.__ofertas.find_one(query)
        return result