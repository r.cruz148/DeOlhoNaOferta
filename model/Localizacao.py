# coding=UTF8
__author__ = 'Ronan Cruz'

class Localizacao(object):

    def __init__(self, latitude, longitude, endereco=''):
        super(Localizacao, self).__init__()
        self.__latitude = latitude
        self.__longitude = longitude
        self.__endereco = endereco

    def getLatitude(self):
        return self.__latitude

    def getLongitude(self):
        return self.__longitude

    def getEndereco(self):
        return self.__endereco

    def __dict__(self):
        d = {}
        d['latitude'] = self.__latitude
        d['longitude'] = self.__longitude
        d['endereco'] = self.__endereco
        return d