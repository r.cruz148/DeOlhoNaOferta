# coding=UTF8
__author__ = 'Ronan Cruz'

from bson.json_util import dumps

class View(object):

    def __init__(self):
        super(View, self).__init__()
        self.__status = False
        self.__mensagem = ''
        self.__result = None

    def atualizar(self, status, mensagem, result=None):
        self.__status = status
        self.__mensagem = mensagem
        self.__result = result

    def gerarJSONSimples(self):
        d = {}
        d['status'] = self.__status
        d['mensagem'] = self.__mensagem
        return dumps(d, ensure_ascii=False)

    def gerarJSONCompleto(self):
        d = {}
        d['status'] = self.__status
        d['mensagem'] = self.__mensagem
        d['JSON'] = self.__result
        return dumps(d, ensure_ascii=False)
